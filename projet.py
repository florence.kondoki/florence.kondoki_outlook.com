
##test
print ('________________________')
print ('TESTS')
print ('________________________')
s = 'bonjour'
for c in s:
    if c in ['o', 'r']:
        print(c)

table = {'Sjoerd': 4127, 'Jack': 4098, 'Dcab': 7678}
for name, phone in table.items():
   print(f'{name:15} ===> {phone:10d}')

print ('________________________')
print ('________________________')


vetement = {'pantalon' : 13, 't-shirt': 8, 'veste': 40, 'manteau': 70}

##variable d'attributs
taille = {t1:= 'XS',t2:= 'S',t3:= 'M',t4:= 'L',t5:= 'XL'}
couleur = {c1:= 'rouge', c2:='vert', c3:='bleu'}
prix = {pr1:= 13, pr2:=8, pr3:=40, pr4:=70}

##objets
print ('________________________')
print ('PANTALONS')
print ('________________________')
pantalon = (pa1:=('chino', t2, c1, pr1), pa2:=('jean', t4, c3, pr1))
print (pantalon)
print (pa1)

print ('________________________')
print ('T-SHIRTS')
print ('________________________')
tshirt = (t1:=('uni', t1, c1, pr2), t2:=('chemise', t1, c2, pr3))
print (tshirt)
print (t1)

print ('________________________')
print ('VESTES')
print ('________________________')
veste = (v1:=('trench', t5, c1, pr4), v2:=('bomber', t1, c2, pr3))
print (veste)
print (v1)

print ('________________________')
print ('MANTEAUX')
print ('________________________')
manteau = (m1:=('laine', t5, c1, pr4), m2:=('parka', t1, c2, pr4))
print (manteau)
print (m1)

print ('________________________')
print ('RAYONS')
print ('________________________')
rayons_tshirt = set({pantalon, tshirt, veste, manteau})

##panier
panier1 = {pa2, t1, v2}
print (panier1)

#actions sur mon panier
panier2 = []

##creation de fonctions
#def plus():
    #print('hello')

print ('________________________')
print ('lecture CSV')
print ('________________________')
import csv;

mon_fichier = open ("clients.csv")
myReader = csv.reader(mon_fichier)
for row in myReader:
    print(row)


#### mon projet
print ('________________________')
print ('MON PROJET')
print ('________________________')

#Données
client1 = {"ID":1, "Nom": 'DUPONT', "Age": 55, "Status": 'married', "Contrat": 'CDI', "Residence": 'OWN', "Checking Balance": 5489.81,"Saving Balance": 234896, "loan amount": 234986, "Defaulted": 'no'}
client2 = {"ID":2, "Nom": 'SNOW', "Age": 26, "Status": 'single', "Contrat": 'CDD', "Residence": 'OWN', "Checking Balance": 2279.24,"Saving Balance": 60542, "loan amount": 68746, "Defaulted": 'no'}
client3 = {"ID":3, "Nom": 'TARGARYEN', "Age": 34, "Status": 'single', "Contrat": 'CDI', "Residence": 'OWN', "Checking Balance": -400.81,"Saving Balance": 5500, "loan amount": 60542, "Defaulted": 'no'}
client4 = {"ID":4, "Nom": 'DAENERYS', "Age": 49, "Status": 'single', "Contrat": 'CDI', "Residence": 'OWN', "Checking Balance": 1879.32,"Saving Balance": 41623, "loan amount": 5500, "Defaulted": 'no'}
client5 = {"ID":5, "Nom": 'CERSEI', "Age": 23, "Status": 'married', "Contrat": 'CDD', "Residence": 'RENT', "Checking Balance": -1310.57,"Saving Balance": 1000, "loan amount": 41623, "Defaulted": 'yes'}
client6 = {"ID":6, "Nom": 'LANNISTER', "Age": 41, "Status": 'married', "Contrat": 'CDI', "Residence": 'RENT', "Checking Balance": -368.14,"Saving Balance": 2000, "loan amount": 1000, "Defaulted": 'yes'}
client7 = {"ID":7, "Nom": 'ARYA', "Age": 39, "Status": 'single', "Contrat": 'CDI', "Residence": 'OWN', "Checking Balance": 32.62,"Saving Balance": 68743, "loan amount": 2000, "Defaulted": 'no'}
client8 = {"ID":8, "Nom": 'STARK', "Age": 42, "Status": 'married', "Contrat": 'CDD', "Residence": 'RENT', "Checking Balance": 1963.55,"Saving Balance": 2000, "loan amount": 45000, "Defaulted": 'no'}
client9 = {"ID":9, "Nom": 'MORMONT', "Age": 56, "Status": 'single', "Contrat": 'CDI', "Residence": 'RENT', "Checking Balance": 0,"Saving Balance": 45000, "loan amount": 350000, "Defaulted": 'no'}
client10 = {"ID":10, "Nom": 'GREY', "Age": 44, "Status": 'married', "Contrat": 'CDI', "Residence": 'OWN', "Checking Balance": 1652.46,"Saving Balance": 86496, "loan amount": 86496, "Defaulted": 'yes'}
client11 = {"ID":11, "Nom": 'DUMAS', "Age": 22, "Status": 'single', "Contrat": 'CDD', "Residence": 'RENT', "Checking Balance": 2636.76,"Saving Balance": 36795, "loan amount": 36795, "Defaulted": 'no'}

listclient = [client1,client2,client3,client4,client5,client6,client7,client8,client9,client10,client11]

#Affichage des données
print('')
print ('Données clients :')
print (listclient)

#Client ayant un solde inférieur à 0
print('')
print ('Solde à découvert :')
for x in listclient :
    if x["Checking Balance"]<0:
        print('Attention client '+x["Nom"]+' à découvert')

#Calcul de l'age moyen des clients
print('')
print ('Age moyen des clients:')
somme = 0
for i in listclient :
    somme = somme + i["Age"]
   
moyenne = somme/len(listclient)
print(round(moyenne))

#calcul du total des avoir clients
print('')
print ('total des avoirs:')
for x in listclient :
    total_avoir = x["Checking Balance"] + x["Saving Balance"]
    print (x["Nom"] + ': ' + str(total_avoir))

#Score
print('')
    #ajout d'un élément score dans les dictionnaires clients
client1['Score'] = 0
print (client1)

for x in listclient :
    x['Score'] = 0
    print (x['Nom']+ ': '+ str(x['Score']))


    #definition des criter de scoring
criteria = {"Age": 30, "Status": 'married', "Contrat": 'CDI', "Residence": 'OWN', "Checking Balance": 0,"Saving Balance": 0, "loan amount": 5000, "Defaulted": 'no'}

print('')

print ('Après calcul du score selon les critères : ')
for x in listclient :
    if x["Age"]>criteria["Age"]:
        x["Score"] = x["Score"]+1
        if x["Status"]==criteria["Status"]:
            x["Score"] = x["Score"]+1
            if x["Contrat"]==criteria["Contrat"]:
                x["Score"] = x["Score"]+1
                if x["Residence"]==criteria["Residence"]:
                    x["Score"] = x["Score"]+1
                    if x["Checking Balance"]>criteria["Checking Balance"]:
                        x["Score"] = x["Score"]+1
                        if x["Saving Balance"]>criteria["Saving Balance"]:
                            x["Score"] = x["Score"]+1
                            if x["loan amount"]>criteria["loan amount"]:
                                x["Score"] = x["Score"]+1
                                if x["Defaulted"]==criteria["Defaulted"]:
                                    x["Score"] = x["Score"]+1
   
    print (x['Nom']+ ': '+ str(x['Score']))

from tkinter import * 
from tkinter import messagebox
print ('')
print ('Faible capacités de remboursement pour les clients suivants :')  
for x in listclient :
    if x["Score"]<2:
        root = Tk() 
        root.geometry("300x200") 
          
        w = Label(root, text ='Clients non solvables', font = "50")  
        w.pack() 
          
        messagebox.showwarning("Warning", x['Nom']+' est à risques')


        print (x['Nom'])
          
        root.mainloop()  





    
